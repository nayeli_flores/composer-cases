import {post, get} from '../../common/request';

import {seller} from "./src/seller";
import {buyerElevator} from "./src/buyer_elevator";
import {getContract} from "./src/contract";
import {getReceivingTicket} from "./src/transaction-in";
import {crsApi} from "../../common/settings";
import {initCommodities} from "../../common/commodities";



(async () => {
    console.log(`Environment: ${crsApi}`);

    await initCommodities();

    // Seller
    console.log(`Creating Seller (${seller.email})...`);
    await post('Member', seller);

    // Buyer and Elevator
    console.log(`Creating Buyer and Elevator (${buyerElevator.email})...`);
    await post('Member', buyerElevator);

    // Contract
    const contract = getContract(seller.email, buyerElevator.email);
    console.log(`Creating Contract (${contract.id})...`);
    await post('Contract', contract);
    console.log(`✅  Price per quintal $${(contract.pricing.price_per_hundred / 100).toLocaleString()} HNL`);




    // Receiving Ticket
    console.log('========== 🚛 LOAD 1 ==========');
    const receivingTicket1 = getReceivingTicket(seller.email, buyerElevator.email, contract.id);
    receivingTicket1.characteristics[0].value = contract.pricing.price_per_hundred;
    console.log(`Sending receivingTicket1 (${receivingTicket1.id})...`);
    const res1 = await post('ClearInventoryTransaction', receivingTicket1);
    console.log(`ClearInventoryTransaction response (${JSON.stringify(res1)})...`);

    // load1
    try {
        const load1 = await get('Load', receivingTicket1.id);
        console.log(`✅  Load 1 created successfully (${receivingTicket1.id})...`);
        console.log(`🔰  Weight: ${(load1.contract_dry_weight).toLocaleString()} lb`);
        console.log(`🔰  Price: $ ${(load1.value / 100).toLocaleString()} HNL`);

    } catch (e) {
        console.log(`❌ load1 was not created. Checking in Inventory (${receivingTicket1.id})...`);

        try {
            const inventoryload1 = await get('ClearInventory', receivingTicket1.id);
            console.log(`✅ load1 is inside the inventory (${receivingTicket1.id})...`);
        } catch (e) {
            console.log(`❌ load1 is not in the inventory (${receivingTicket1.id})...`);
        }
    }
    


    // Receiving Ticket
    console.log('========== 🚛 LOAD 2 ==========');
    const receivingTicket2 = getReceivingTicket(seller.email, buyerElevator.email, contract.id);
    console.log(`Sending receivingTicket2 (${receivingTicket2.id})...`);
    const res2 = await post('ClearInventoryTransaction', receivingTicket2);
    console.log(`ClearInventoryTransaction response (${JSON.stringify(res2)})...`);

    // load2
    try {
        const load2 = await get('Load', receivingTicket2.id);
        console.log(`✅  Load 2 created successfully (${receivingTicket2.id})...`);
        console.log(`🔰  Weight: ${(load2.contract_dry_weight).toLocaleString()} lb`);
        console.log(`🔰  Price replacement: $ ${(load2.characteristics.find((ch: { id: string; }) => ch.id === 'Precio de la Plaza').value / 100).toLocaleString()} HNL`);
        console.log(`🔰  Price: $ ${(load2.value / 100).toLocaleString()} HNL`);
    } catch (e) {
        console.log(`❌ load2 was not created. Checking in Inventory (${receivingTicket2.id})...`);

        try {
            const inventoryload2 = await get('ClearInventory', receivingTicket2.id);
            console.log(`✅ load2 is inside the inventory (${receivingTicket2.id})...`);
        } catch (e) {
            console.log(`❌ load2 is not in the inventory (${receivingTicket2.id})...`);
        }
    }


})()

