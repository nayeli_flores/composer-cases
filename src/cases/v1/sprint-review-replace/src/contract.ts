import {getRandomInt} from "../../../common/helpers";

export const getContract = (sellerEmail: string, buyerElevatorEmail: string) => {
    return {
        "$class": "io.grainchain.Contract",
        "id": `D${getRandomInt(10000, 99999)}`,
        "contract_type": "Buy",
        "start_date": "2021-09-01T05:00:00.000Z",
        "end_date": "2021-10-31T23:59:59.000Z",
        "weight": 1000000,
        "weight_fullfilled": 0,
        "weight_threshold": 1010000,
        "commodity": "resource:io.grainchain.Commodity#1",
        "certification_by": "None",
        "first_payee": "Elevator",
        "status": "Open",
        "fullFilledCommodityObligationRecords": [],
        "pricing": {
            "$class": "io.grainchain.PricingSchema",
            "weight_mode": "Dry",
            "price_per_hundred": 378,
            "bushel_weight": false,
            "pricing_type": "HNL",
            "price_input": 3.5
        },
        "moisture_table": [],
        "seller": `resource:io.grainchain.Member#${sellerEmail}`,
        "elevator": `resource:io.grainchain.Member#${buyerElevatorEmail}`,
        "buyer": `resource:io.grainchain.Member#${buyerElevatorEmail}`,
        "weight_unit": "quintal",
        "settle_at": `resource:io.grainchain.Member#${buyerElevatorEmail}`,
        "characteristics": [],
        "destination": [
            {
                "$class": "io.grainchain.Destination",
                "elevator": `resource:io.grainchain.Member#${buyerElevatorEmail}`,
                "in_fee": 0,
                "out_fee": 0,
                "storage_fee": 0,
                "control_fee": 0,
                "approved": true,
                "in_fee_x100": 0,
                "out_fee_x100": 0,
                "storage_fee_x100": 0,
                "control_fee_x100": 0,
                "in_fee_currency": "0",
                "out_fee_currency": "0",
                "storage_fee_currency": "0",
                "control_fee_currency": "0",
                "in_fee_unit": "0",
                "out_fee_unit": "0",
                "storage_fee_unit": "0",
                "control_fee_unit": "0",
                "initials": "Porfirio Bustamante Manzano",
                "approved_at": "2021-09-08T21:01:48.445Z",
                "is_aggregator": true
            }
        ],
        "payee_definition": [],
        "approved_by_seller": true,
        "approved_by_buyer": true,
        "weight_type": "cars",
        "estimatedContractValue": 3500000,
        "fullFilledCommodityObligation": 0,
        "buyerPaidObligation": 0,
        "PaidObligationRecords": [],
        "buyerUnPaidObligation": 0,
        "sellerPaidObligation": 0,
        "sellerUnPaidObligation": 0,
        "automatic_assignment": false,
        "weight_underthreshold": 990000,
        "totalSettledCommodity": 0,
        "settledGrainPay": 0,
        "SettledGrainPayRecord": [],
        "payment": {
            "$class": "io.grainchain.PaymentDefinition",
            "method": "Manual",
            "status": "Unpaid",
            "processor": "Dwolla"
        },
        "weight_input": 10,
        "buyer_initials": "Porfirio Bustamante Manzano",
        "seller_initials": "Admin GC",
        "buyer_approved_at": "2021-09-08T21:01:48.445Z",
        "seller_approved_at": "2021-09-08T21:02:25.411Z",
        "approvalStatus": "Approved",
        "creator": `resource:io.grainchain.Member#${buyerElevatorEmail}`,
        "grainchain_fee_percentage": 0,
        "lien_holders": [],
        "weight_threshold_input": 1,
        "weight_underthreshold_input": 1,
        "first_lien_holders": [],
        "assessment": 0,
        "assessment_definition": {
            "$class": "io.grainchain.AssessmentDefinition",
            "type": "None",
            "value": 0,
            "description": "N/A"
        },
        "gc_fee": {
            "$class": "io.grainchain.GCFee",
            "payer": "Buyer",
            "percentage": 0
        },
        "quality_specs": [
            {
                "$class": "io.grainchain.CharacteristicSchema",
                "id": "Precio de la Plaza",
                "name": "Precio de la Plaza",
                "criteria_type": "Replace",
                "criteria": [
                    "{\"downLimit\":10,\"upLimit\":5000,\"criteria\":[{\"action\":\"replace\",\"variable\":\"price\"}]}"
                ],
                "certification_type": "SiloSys",
                "mandatory": false
            }
        ],
        "bushel_size": 56,
        "quintal_size": 100
    }

}
