import {generateEmail} from "../../../common/helpers";

export const buyerElevator = {
    "$class": "io.grainchain.Member",
    "email": generateEmail(),
    "type": "Company",
    "firstName": "Porfirio",
    "lastName": "Bustamante Manzano",
    "phone": "+50495175482",
    "ssn": "0000000000007",
    "dateOfBirth": "2003-09-03T00:00:00.000Z",
    "GrainPayBalance": 0,
    "address": [
        {
            "$class": "io.grainchain.UnitedStatesAddress",
            "street_1": "Paseo de la Reforma #455",
            "city": "Choluteca",
            "state": "Choluteca",
            "country": "Honduras",
            "zipcode": "34784",
            "lat": 0,
            "lng": 0
        }
    ],
    "businessInformation": {
        "$class": "io.grainchain.BusinessInformation",
        "businessName": "Café Quintal",
        "businessClassification": ""
    },
    "contacts": [],
    "deposits": [],
    "withdrawals": [],
    "incoming_transfers": [],
    "outgoing_transfers": [],
    "stage": 3,
    "status": "Approved",
    "participantType": "Elevator",
    "activate2FA": false,
    "accountStatus": "PendingBankAccount",
    "hasActiveContracts": false,
    "settings": {
        "$class": "io.grainchain.MemberSettings",
        "grainchain_fee_percentage": 0,
        "auto_withdrawal": true
    }
}
