import {post, get} from '../../common/request';
import {seller} from "./src/seller";
import {buyer} from "./src/buyer";
import {elevator} from "./src/elevator";
import {getContract} from "./src/contract";
import {getLoad} from "./src/load";
import {getBatch} from "./src/batch";
import {crsApi} from "../../common/settings";
import {initCommodities} from "../../common/commodities";

(async () => {
    await initCommodities();

    console.log(`Environment: ${crsApi}`);

    // Seller
    console.log(`Creating Seller (${seller.email})...`);
    await post('Member', seller);

    // Buyer
    console.log(`Creating Buyer (${buyer.email})...`);
    await post('Member', buyer);

    // Elevator
    console.log(`Creating Elevator (${elevator.email})...`);
    await post('Member', elevator);

    // Contract
    const contract = getContract(seller.email, buyer.email, elevator.email);
    console.log(`Creating Contract (${contract.id})...`);
    await post('Contract', contract);

    // Load
    const load = getLoad(seller.email, buyer.email, contract.id);
    console.log(`Creating Load (${load.loadId})...`);
    await post('Load', load);

    // Batch
    const batch: any = getBatch(contract.id, load.loadId);
    console.log(`Creating Batch (${batch.id})...`);
    await post('Batch', batch);

    // Settlement
    const settlementData = {id: contract.id, type: 'contract', tmstmp: new Date()}
    console.log(`Doing Settlement...`, JSON.stringify({id: contract.id, type: 'contract', tmstmp: new Date()}));
    await post('SettlementTransaction', settlementData);
})()

