import {getRandomInt} from "../../../common/helpers";
import {elevator} from "./elevator";


export const getContract = (sellerEmail: string, buyerEmail: string, elevatorEmail: string) => {
    return {
        "$class": "io.grainchain.Contract",
        "id": `D${getRandomInt(10000, 99999)}`,
        "contract_type": "Sell",
        "start_date": "2021-10-01T05:00:00.000Z",
        "end_date": "2021-10-31T23:59:59.000Z",
        "weight": 100000,
        "weight_fullfilled": 0,
        "weight_threshold": 105000,
        "commodity": "resource:io.grainchain.Commodity#11",
        "certification_by": "None",
        "first_payee": "Elevator",
        "status": "Open",
        "fullFilledCommodityObligationRecords": [],
        "pricing": {
            "$class": "io.grainchain.PricingSchema",
            "weight_mode": "AsIs",
            "price_per_hundred": 12000,
            "bushel_weight": false,
            "pricing_type": "HNL",
            "price_input": 120
        },
        "moisture_table": [],
        "seller": `resource:io.grainchain.Member#${sellerEmail}`,
        "elevator": `resource:io.grainchain.Member#${elevatorEmail}`,
        "buyer": `resource:io.grainchain.Member#${buyerEmail}`,
        "weight_unit": "quintal",
        "settle_at": `resource:io.grainchain.Member#${elevatorEmail}`,
        "characteristics": [],
        "destination": [
            {
                "$class": "io.grainchain.Destination",
                "elevator": `resource:io.grainchain.Member#${elevatorEmail}`,
                "in_fee": 0,
                "out_fee": 0,
                "storage_fee": 0,
                "control_fee": 0,
                "approved": true,
                "in_fee_x100": 0,
                "out_fee_x100": 0,
                "storage_fee_x100": 0,
                "control_fee_x100": 0,
                "in_fee_currency": "0",
                "out_fee_currency": "0",
                "storage_fee_currency": "0",
                "control_fee_currency": "0",
                "in_fee_unit": "0",
                "out_fee_unit": "0",
                "storage_fee_unit": "0",
                "control_fee_unit": "0",
                "initials": "Porfirio Bustamante Manzano",
                "approved_at": "2021-09-08T21:01:48.445Z",
                "is_aggregator": true
            }
        ],
        "payee_definition": [],
        "approved_by_seller": true,
        "approved_by_buyer": true,
        "weight_type": "quintal",
        "estimatedContractValue": 12000000,
        "fullFilledCommodityObligation": 2396,
        "buyerPaidObligation": 0,
        "PaidObligationRecords": [],
        "buyerUnPaidObligation": 0,
        "sellerPaidObligation": 0,
        "sellerUnPaidObligation": 0,
        "automatic_assignment": false,
        "weight_underthreshold": 95000,
        "totalSettledCommodity": 0,
        "settledGrainPay": 0,
        "SettledGrainPayRecord": [],
        "payment": {
            "$class": "io.grainchain.PaymentDefinition",
            "method": "Terms",
            "periodicity": "Daily",
            "status": "Unpaid",
            "processor": "Dwolla"
        },
        "weight_input": 1000,
        "buyer_initials": "Flor Aguilar",
        "seller_initials": "Carla Paulina Bravo Juarez",
        "buyer_approved_at": "2021-09-10T21:50:02.945Z",
        "seller_approved_at": "2021-09-10T21:39:56.661Z",
        "approvalStatus": "Approved",
        "creator": `resource:io.grainchain.Member#${sellerEmail}`,
        "grainchain_fee_percentage": 0,
        "lien_holders": [],
        "weight_threshold_input": 5,
        "weight_underthreshold_input": 5,
        "first_lien_holders": [],
        "assessment": 0,
        "assessment_definition": {
            "$class": "io.grainchain.AssessmentDefinition",
            "type": "None",
            "value": 0,
            "description": "N/A"
        },
        "gc_fee": {
            "$class": "io.grainchain.GCFee",
            "payer": "Buyer",
            "percentage": 0
        },
        "quality_specs": [],
        "bushel_size": 56,
        "quintal_size": 100
    }

}
