import {generateEmail} from "../../../common/helpers";

export const seller = {
    "$class": "io.grainchain.Member",
    "email": generateEmail(),
    "type": "Company",
    "firstName": "Carla Paulina",
    "lastName": "Bravo Juarez",
    "phone": "+50438922926",
    "ssn": "3045679908586",
    "dateOfBirth": "1989-10-12T00:00:00.000Z",
    "GrainPayBalance": 0,
    "address": [
        {
            "$class": "io.grainchain.UnitedStatesAddress",
            "street_1": "Calle Trece No. 777 Col. centro",
            "city": "Santa Fe",
            "state": "Colón",
            "country": "Honduras",
            "zipcode": "06893",
            "lat": 0,
            "lng": 0
        }
    ],
    "businessInformation": {
        "$class": "io.grainchain.BusinessInformation",
        "businessName": "Agroindustrias San Jose SA de CV",
        "businessClassification": ""
    },
    "contacts": [],
    "deposits": [],
    "withdrawals": [],
    "incoming_transfers": [],
    "outgoing_transfers": [],
    "stage": 3,
    "status": "Approved",
    "participantType": "Seller",
    "activate2FA": false,
    "accountStatus": "Active",
    "hasActiveContracts": false,
    "settings": {
        "$class": "io.grainchain.MemberSettings",
        "grainchain_fee_percentage": 0,
        "auto_withdrawal": true
    }
}
