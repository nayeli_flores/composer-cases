import {getRandomInt} from "../../../common/helpers";

export const getBatch = (contractId: string, loadId: string) => {
    return {
        "$class": "io.grainchain.Batch",
        "id": `${getRandomInt(100, 999)}-${getRandomInt(10000, 99999)}`,
        "status": "Paid",
        "batchType": "Auto",
        "contract": `resource:io.grainchain.Contract#${contractId}`,
        "loads": [
            `resource:io.grainchain.Load#${loadId}`
        ],
        "processor": "Bantrab",
        "withdrawalId": "9548329d-8d34-4af1-bd90-8f9c1957511d",
        "totalAmount": 240000,
        "createdAt": "2021-09-10T22:16:15.000Z",
        "liens": [],
        "assessment": 0,
        "subTotalAmount": 240000,
        "disbursement": [],
        "grainchain_fee": 0,
        "currency": "HNL",
        "discounts": [],
        "payment": {
            "$class": "io.grainchain.BatchPaymentSchema",
            "method": "Check",
            "additional_info": [
                {
                    "$class": "io.grainchain.PaymentInfoSchema",
                    "reference": "09721232",
                    "email": "cpbravo@grainchain.dev",
                    "source": {
                        "$class": "io.grainchain.BankInfoSchema",
                        "bank_name": "BANCO POPULAR",
                        "bank_id": "33"
                    }
                }
            ]
        }
    }
}
