import {generateEmail} from "../../../common/helpers";

export const buyer = {
    "$class": "io.grainchain.Member",
    "email": generateEmail(),
    "type": "Individual",
    "firstName": "Flor",
    "lastName": "Aguilar ",
    "phone": "+10099887711",
    "ssn": "9999922211333",
    "dateOfBirth": "2003-09-01T00:00:00.000Z",
    "GrainPayBalance": 0,
    "address": [
        {
            "$class": "io.grainchain.UnitedStatesAddress",
            "street_1": "Sirenas",
            "city": "Caridad",
            "state": "Valle",
            "country": "Honduras",
            "zipcode": "99821",
            "lat": 0,
            "lng": 0
        }
    ],
    "contacts": [],
    "deposits": [],
    "withdrawals": [],
    "incoming_transfers": [],
    "outgoing_transfers": [],
    "stage": 3,
    "status": "Approved",
    "participantType": "Buyer",
    "activate2FA": false,
    "accountStatus": "Active",
    "hasActiveContracts": false,
    "settings": {
        "$class": "io.grainchain.MemberSettings",
        "grainchain_fee_percentage": 0,
        "auto_withdrawal": true
    }
}
