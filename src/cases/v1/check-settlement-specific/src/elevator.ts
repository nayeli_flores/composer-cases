import {generateEmail} from "../../../common/helpers";

export const elevator = {
    "$class": "io.grainchain.Member",
    "email": generateEmail(),
    "type": "Individual",
    "firstName": "Xavier",
    "lastName": "Barrera ",
    "phone": "+529900008811",
    "ssn": "1555552211111",
    "dateOfBirth": "2003-09-10T00:00:00.000Z",
    "GrainPayBalance": 0,
    "address": [
        {
            "$class": "io.grainchain.UnitedStatesAddress",
            "street_1": "Torres",
            "city": "Utila",
            "state": "Islas de la Bahía",
            "country": "Honduras",
            "zipcode": "22211",
            "lat": 0,
            "lng": 0
        }
    ],
    "contacts": [],
    "deposits": [],
    "withdrawals": [],
    "incoming_transfers": [],
    "outgoing_transfers": [],
    "stage": 3,
    "status": "Approved",
    "participantType": "Elevator",
    "activate2FA": false,
    "accountStatus": "Active",
    "hasActiveContracts": false,
    "settings": {
        "$class": "io.grainchain.MemberSettings",
        "grainchain_fee_percentage": 0,
        "auto_withdrawal": true
    }
}
