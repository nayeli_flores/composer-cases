import {getRandomInt} from "../../../common/helpers";

export const getLoad = (sellerEmail: string, elevatorEmail: string, contractId: string) => {
    return {
        "$class": "io.grainchain.Load",
        "loadId": `${getRandomInt(0, 9)}.${getRandomInt(1000, 9999)}.${getRandomInt(1000, 9999)}`,
        "load_date": "2021-09-10T22:11:34.182Z",
        "commodity": "resource:io.grainchain.Commodity#11",
        "elevator": `resource:io.grainchain.Member#${elevatorEmail}`,
        "seller": `resource:io.grainchain.Member#${sellerEmail}`,
        "weight": 2000,
        "silo_dry_weight": 1914,
        "contract_dry_weight": 2000,
        "temperature": 0,
        "moisture": 0,
        "test_weight": 26,
        "value": 240000,
        "status": "Unpaid",
        "test_serial": "test_serial",
        "contract": `resource:io.grainchain.Contract#${contractId}`,
        "certificate_url": "grainchain.io",
        "batched": true,
        "original_ticket": "15516bf8-397",
        "trailer_plate": "011-840-5097",
        "characteristics": [],
        "breakdown": [],
        "deliveryId": "d3931f60-a18",
        "receiving_ticket": "7b3387a4-6b1",
        "currency": "HNL"
    }
}
