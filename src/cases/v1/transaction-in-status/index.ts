import {post, get} from '../../common/request';

import {seller} from "./src/seller";
import {buyerElevator} from "./src/buyer_elevator";
import {getContract} from "./src/contract";
import {getReceivingTicket} from "./src/transaction-in";
import {crsApi} from "../../common/settings";
import {initCommodities} from "../../common/commodities";



(async () => {
    await initCommodities();

    console.log(`Environment: ${crsApi}`);

    // Seller
    console.log(`Creating Seller (${seller.email})...`);
    await post('Member', seller);

    // Buyer and Elevator
    console.log(`Creating Buyer and Elevator (${buyerElevator.email})...`);
    await post('Member', buyerElevator);

    // Contract
    const contract = getContract(seller.email, buyerElevator.email);
    console.log(`Creating Contract (${contract.id})...`);
    await post('Contract', contract);

    // Receiving Ticket
    const receivingTicket = getReceivingTicket(seller.email, buyerElevator.email, contract.id);
    console.log(`Sending ReceivingTicket (${receivingTicket.id})...`);
    const res = await post('ClearInventoryTransaction', receivingTicket);
    console.log(`ClearInventoryTransaction response (${JSON.stringify(res)})...`);

    // Load
    try {
        const load = await get('Load', receivingTicket.id);
        console.log(`✅ Load created successfully (${receivingTicket.id})...`);

    } catch (e) {
        console.log(`❌ Load was not created. Checking in Inventory (${receivingTicket.id})...`);

        try {
            const inventoryLoad = await get('ClearInventory', receivingTicket.id);
            console.log(`✅ Load is inside the inventory (${receivingTicket.id})...`);
        } catch (e) {
            console.log(`❌ Load is not in the inventory (${receivingTicket.id})...`);
        }
    }


})()

