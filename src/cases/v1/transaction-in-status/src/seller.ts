import {generateEmail} from "../../../common/helpers";

export const seller = {
    "$class": "io.grainchain.Member",
    "email": generateEmail(),
    "type": "Individual",
    "firstName": "Admin",
    "lastName": "GC ",
    "phone": "+527745885445",
    "ssn": "0000000000002",
    "dateOfBirth": "1981-06-12T00:00:00.000Z",
    "GrainPayBalance": 0,
    "address": [
        {
            "$class": "io.grainchain.UnitedStatesAddress",
            "street_1": "Asperiores qui dolor",
            "street_2": "Do dolorem optio fa",
            "city": "Choluteca",
            "state": "Choluteca",
            "country": "Honduras",
            "zipcode": "17878",
            "lat": 0,
            "lng": 0
        }
    ],
    "contacts": [],
    "deposits": [],
    "withdrawals": [],
    "incoming_transfers": [],
    "outgoing_transfers": [],
    "stage": 3,
    "status": "Approved",
    "participantType": "Seller",
    "activate2FA": false,
    "accountStatus": "PendingBankAccount",
    "hasActiveContracts": false,
    "settings": {
        "$class": "io.grainchain.MemberSettings",
        "grainchain_fee_percentage": 0,
        "auto_withdrawal": true
    }
}
