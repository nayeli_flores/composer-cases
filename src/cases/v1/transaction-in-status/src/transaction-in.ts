import {getRandomInt} from "../../../common/helpers";

export const getReceivingTicket = (sellerEmail: string, elevatorEmail: string, contractId: string) => {
    return {
        "id": `${getRandomInt(0, 9)}.${getRandomInt(1000, 9999)}.${getRandomInt(1000, 9999)}`,
        "receiving_date":"2021-10-01T21:02:31.246Z",
        "seller":`resource:io.grainchain.Member#${sellerEmail}`,
        "elevator":`resource:io.grainchain.Member#${elevatorEmail}`,
        "commodity":"resource:io.grainchain.Commodity#22",
        "weight":getRandomInt(50000, 100000),
        "silo_dry_weight":getRandomInt(50000, 100000),
        "test_weight":getRandomInt(1, 100),
        "test_serial":"test_serial",
        "moisture":0,
        "certificate_url":"grainchain.io",
        "gc_contract":contractId,
        "original_ticket":"",
        "driver_name":"Olivia Carpentier",
        "truck_plate":"06-78-34-59-08",
        "fieldTicketId":"e02c94bb-f0a",
        "blockId":"88316fac-521",
        "characteristics":[
            {
                "id": "Moisture",
                "value":15.5
            }
        ]
    }
}
