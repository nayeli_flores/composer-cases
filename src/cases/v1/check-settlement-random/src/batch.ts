import {getRandomInt} from "../../../common/helpers";

export const getBatch = (contractId: string, loadId: string) => {
    return {
        "$class": "io.grainchain.Batch",
        "id": `${getRandomInt(100, 999)}-${getRandomInt(10000, 99999)}`,
        "status": "Paid",
        "batchType": "Auto",
        "contract": `resource:io.grainchain.Contract#${contractId}`,
        "loads": [
            `resource:io.grainchain.Load#${loadId}`
        ],
        "processor": "Bantrab",
        "withdrawalId": "7a081586-e8b7-45b5-8247-12fbc12d9258",
        "totalAmount": 48950,
        "createdAt": "2021-09-08T16:55:07.000Z",
        "liens": [],
        "assessment": 0,
        "subTotalAmount": 50000,
        "disbursement": [],
        "grainchain_fee": 0,
        "currency": "HNL",
        "discounts": [
            {
                "$class": "io.grainchain.BatchDiscount",
                "description": "Pago por Maniobra",
                "amount": 10.5,
                "percentage": 10
            }
        ],
        "payment": {
            "$class": "io.grainchain.BatchPaymentSchema",
            "method": "Cash",
            "additional_info": [
                {
                    "$class": "io.grainchain.PaymentInfoSchema",
                    "reference": "CHEQUE1",
                    "photo": "staging-honduras/batch/6138eaeb0948a/checks/d89c96bf-8816-4c5a-9480-71f6ddc37f5b.png",
                    "email": "laguilar@grainchain.dev"
                },
                {
                    "$class": "io.grainchain.PaymentInfoSchema",
                    "reference": "CHEQUE2",
                    "photo": "staging-honduras/batch/6138eaeb0948a/checks/e2240480-2f51-44d6-8931-c121ef48affc.png",
                    "email": "ibarrera@grainchain.dev"
                }
            ]
        }
    }
}
