import {getRandomInt} from "../../../common/helpers";

export const getLoad = (sellerEmail: string, elevatorEmail: string, contractId: string) => {
    return {
        "$class": "io.grainchain.Load",
        "loadId": `${getRandomInt(0, 9)}.${getRandomInt(1000, 9999)}.${getRandomInt(1000, 9999)}`,
        "load_date": "2021-09-08T21:03:01.937Z",
        "commodity": "resource:io.grainchain.Commodity#1",
        "elevator": `resource:io.grainchain.Member#${elevatorEmail}`,
        "seller": `resource:io.grainchain.Member#${sellerEmail}`,
        "weight": 58073,
        "silo_dry_weight": 57578,
        "contract_dry_weight": 58073,
        "temperature": 0,
        "moisture": 0,
        "test_weight": 45,
        "value": 203256,
        "status": "Unpaid",
        "test_serial": "test_serial",
        "contract": `resource:io.grainchain.Contract#${contractId}`,
        "certificate_url": "grainchain.io",
        "batched": false,
        "original_ticket": "526a7d9f-9b5",
        "driver_name": "Dean Banks",
        "truck_plate": "0442-686-720",
        "trailer_plate": "03-6906-8387",
        "characteristics": [
            {
                "$class": "io.grainchain.LoadCharacteristicSchema",
                "id": "Moisture",
                "value": 9
            }
        ],
        "breakdown": [
        ],
        "deliveryId": "cb01b5cf-a50",
        "fieldTicketId": "4cc10949-3f9",
        "currency": "HNL"
    }
}
