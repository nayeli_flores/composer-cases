import {post, get} from '../../common/request';

import {seller} from "./src/seller";
import {buyerElevator} from "./src/buyer_elevator";
import {getContract} from "./src/contract";
import {getLoad} from "./src/load";
import {getBatch} from "./src/batch";
import {crsApi} from "../../common/settings";
import {initCommodities} from "../../common/commodities";
import request from "request";

const validateDeposit = ({payment: {method}}: any, {no_withdrawn_amount}: any) => {
    console.log(`Validating Deposit...`);
    let message: string;
    switch (method) {
        case 'Cash':
        case 'Check':
            message = no_withdrawn_amount ? `❌ The available amount has to be 0, but it is ${no_withdrawn_amount.toLocaleString()}` : `✅ The available amount is 0`;
            console.log(message);
            break;
    }
}


(async () => {
    await initCommodities();

    console.log(`Environment: ${crsApi}`);

    // Seller
    console.log(`Creating Seller (${seller.email})...`);
    await post('Member', seller);

    // Buyer and Elevator
    console.log(`Creating Buyer and Elevator (${buyerElevator.email})...`);
    await post('Member', buyerElevator);

    // Contract
    const contract = getContract(seller.email, buyerElevator.email);
    console.log(`Creating Contract (${contract.id})...`);
    await post('Contract', contract);

    // Load
    const load = getLoad(seller.email, buyerElevator.email, contract.id);
    console.log(`Creating Load (${load.loadId})...`);
    await post('Load', load);

    // Batch
    const batch: any = getBatch(contract.id, load.loadId);
    console.log(`Creating Batch (${batch.id})...`);
    await post('Batch', batch);

    // Settlement
    const settlementData = {id: contract.id, type: 'contract', tmstmp: new Date()}
    console.log(`Doing Settlement...`, JSON.stringify({id: contract.id, type: 'contract', tmstmp: new Date()}));
    await post('SettlementTransaction', settlementData);

    // Deposit
    console.log(`Getting Deposit...`);
    const deposits: any = await get(`Deposit?filter=%7B%22where%22%3A%20%7B%22batch%22%3A%20%22resource%3Aio.grainchain.Batch%23${batch.id}%22%7D%7D`);
    const deposit = deposits[0];
    console.log(`${deposits.length} Deposits found`);
    console.log(`First Deposit:`);
    console.log(`ID: ${deposit.depositId}`);
    console.log(`Recipient: ${deposit.recipient}`);
    console.log(`Status: ${deposit.status}`);
    console.log(`Exchange Type: ${deposit.exchangeType}`);
    if (batch.payment) {
        validateDeposit(batch, deposit);
    }

    // Withdrawal
    const withdrawalId = `${deposit.depositId}-w`;
    try {
        console.log(`Getting Withdrawal...`);
        const withdrawal: any = await get(`Deposit`, withdrawalId);
        console.log(`✅ Withdrawal ID: ${withdrawal.depositId}`);
        if (withdrawal.status !== 'processed') {
            console.log(`❌ Withdrawal is ${withdrawal.status}`)
        }
    } catch (e) {
        console.log(`❌ Withdrawal ${withdrawalId} not found`);
    }

    // Validate Batch
    const batchUpdated = await get('Batch', batch.id);
    const result = (batchUpdated.status === 'Settled') ?  `✅ Batch successfully settled` : `❌ Batch not settled`;
    console.log(result);
})()

