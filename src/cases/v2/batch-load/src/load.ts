import {getRandomInt} from "../../../../common/helpers";

export const getLoad = (sellerId: string, elevatorId: string, contractId: string) => {
    return {
        "id":`${getRandomInt(0, 9)}.${getRandomInt(1000, 9999)}.${getRandomInt(1000, 9999)}`,
        "receiving_at": new Date(),
        "commodity": {
            "name": "Yellow Corn",
            "id": "5f204ca208d9c1ea46483836"
        },
        "contract_id": contractId,
        "characteristics": [
            {
                "id": "23",
                "name": "humedad",
                "value": "2"
            },{
                "id": "22",
                "name": "temperature",
                "value": "8"
            }
        ],
        "weight": {
            "quantity": getRandomInt(50000,100000),
            "measurement_unit": "lbs",
            "measurement_unit_id": "5e1e456c108f8768b13b3214"
        },
        "seller": `${sellerId}`,
        "elevator": {
            "id": `${elevatorId}`,
            "at_receiving": true,
            "at_shipping": false,
            "location": "location_id"
        },
        "is_settlement_location": true,
        "type": "receiving",
        "source": "Cmodity Tools",
        "market_id": "5ec4133943d409692485caf1",
        "source_id": "6537.8226.2.5"
    };
}
