import {getRandomInt} from "../../../../common/helpers";

export const getAgreement = (): any => {
    return {
        "id": `D${getRandomInt(10000, 99999)}`,
        "market_id":"1",
        "weight": {
            "upper_threshold": 1000100,
            "price": 3.75,
            "lower_threshold": 950000,
            "quantity": 1000000,
            "measurement_unit": "lbs",
            "measurement_unit_id": "5e1e456c108f8768b13b3214"
        },
        "thresholds": {
            "minimum_percentage": -25,
            "maximum_percentage": 25,
            "minimum_amount": 7500,
            "maximum_amount": 12500
        },
        "commodity": {
            "name": "Yellow Corn",
            "id": "5f204ca208d9c1ea46483836"
        },
        "pricing": {
            "price": 15,
            "estimated_contract_value": 150000,
            "currency": "USD",
            "currency_id": "5ddf006696e446090bd235b6",
            "measurement_unit": "lbs",
            "measurement_unit_id": "5e1e456c108f8768b13b3214"
        },
        "characteristics": [
            {
                "name": "Humedad",
                "required": true,
                "type": "table",
                "description": "",
                "values": [
                    {
                        "criteria": [
                            {
                                "action": "subtract",
                                "variable": "price",
                                "coefficient": 0.05
                            },
                            {
                                "action": "percentage",
                                "variable": "weight",
                                "coefficient": 100,
                                "coefficient_type": "reversed"
                            }
                        ],
                        "down_limit": 0,
                        "up_limit": 14
                    }
                ]
            },
            {
                "name": "Color",
                "required": true,
                "type": "choice",
                "description": "",
                "values": [
                    {
                        "choice_name": "Pinto verde",
                        "criteria": [
                            {
                                "action": "percentage",
                                "variable": "weight",
                                "coefficient": 4.5,
                                "coefficient_type": "simple"
                            },
                            {
                                "action": "add",
                                "variable": "price",
                                "coefficient": 3,
                                "coefficient_shown": 0.03,
                                "unit_shown": "lbs"
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Diferencia Manual - Honduras",
                "required": false,
                "type": "adjustment",
                "description": ""
            },
            {
                "name": "Diferencia Manual 2 - Honduras",
                "required": false,
                "type": "replacement",
                "description": ""
            },
            {
                "name": "olor",
                "required": false,
                "type": "informative",
                "description": ""
            }
        ],
        "signed_by": "2fa",
        "grainchain_fee": {
            "contract_fee_percentage": 1,
            "payer": [
                {
                    "participant": "buyer",
                    "fee_percentage": 50
                },
                {
                    "participant": "seller",
                    "fee_percentage": 50
                }
            ]
        },
        "participants": [
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "seller",
                "signatures": [
                    {
                        "id":"s1",
                        "request_to_sign_id": "__"
                    }
                ]
            },
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "lien_holder",
                "signatures": [
                    {
                        "id":"s1",
                        "request_to_sign_id": "__"
                    }
                ]
            },
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "lien_holder",
                "signatures": [
                    {
                        "id":"s1",
                        "request_to_sign_id": "__"
                    }
                ]
            },
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "buyer",
                "signatures": [
                    {
                        "id":"s2",
                        "request_to_sign_id": "__"
                    }
                ]
            },
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "destination",
                "signatures": [
                    {
                        "id":"s2",
                        "request_to_sign_id": "__"
                    }
                ],
                "fees": [
                    {
                        "name": "storage",
                        "value": 20293.123 //valor por 100 libras siempre en libras.
                    },
                    {
                        "name": "pest_control",
                        "value": 5
                    },
                    {
                        "name": "in",
                        "value": 2
                    },
                    {
                        "name": "out",
                        "value": 6
                    }
                ],
                "is_aggregator": true,
                "is_settlement_location": true,
                "at_receiving": true,
                "at_shipping": false
            },
            {
                "id": `p_${getRandomInt(10, 99)}`,
                "role": "payee",
                "signatures": [
                    {
                        "id":"s3",
                        "request_to_sign_id": "__"
                    }
                ],
                "is_limited": true,
                "maximum_amount": "",
                "description": "",
                "fee": 178.57142857142856, //por cada 100 libras
                "amount_settled": 0
            }
        ],
        "period": {
            "start_date": "08-15-2021",
            "end_date": "12-07-2021",
            "time_zone": "UTC"
        },
        "assessment": {
            "description": "Retencion A",
            "type": "price",
            "value": 0.08,
            "custom_weight": 56
        },
        "payment": {
            "status": "Unpaid",
            "processor": "Dwolla"
        }
    }
}
