import {getRandomBoolean, getRandomInt} from "../../../../common/helpers";

export const getBatch = (contractId: string, loads: any[]) => {
    const loadIds: string[] = []
    const fee1 = getRandomInt(999, 10000);
    const fee2 = getRandomInt(999, 10000);
    const fee3 = getRandomInt(999, 10000);
    const fees = [
        {
            description: 'Transport',
            type: getRandomBoolean() ? 'add' : 'subtract',
            amount: fee1
        },
        {
            description: 'Rastreo',
            type: getRandomBoolean() ? 'add' : 'subtract',
            amount: fee2
        },
        {
            description: 'Propina',
            type: getRandomBoolean() ? 'add' : 'subtract',
            amount: fee3
        }
    ]
    let subtotal = 0;
    for (const {id, value} of loads) {
        loadIds.push(id);
        subtotal += value
    }
    let total = subtotal;
    for (let {type, amount} of fees) {
        type === 'add' ? total += amount : total -= amount;
    }
    return {
        "id": `B${getRandomInt(10000, 99999)}`,
        "contract_id":contractId,
        "loads": loads.map(({id}) => (id)),
        "amount": {
            fees,
            "subtotal": subtotal,
            "total": total
        }
    }
}
