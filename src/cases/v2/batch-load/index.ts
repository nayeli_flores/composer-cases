import {initCommodities} from "../../../common/commodities";
import {crsApi} from "../../../common/settings";
import {get, post, put} from "../../../common/request";
import {getAgreement} from "./src/agreement";
import {getLoad} from "./src/load";
import {getBatch} from "./src/batch";

const timeout = 3000;

(async () => {
    console.log(`Environment: ${crsApi}`);

    // Agreement
    const agreement = getAgreement();
    console.log(`Creating Agreement (${agreement.id})...`);
    post('Agreement', agreement).then(res0 => {
        setTimeout(() => {
            agreement.status = 'open'
            console.log(`Opening Agreement (${agreement.id})...`);
            put('Agreement', agreement.id, agreement).then(res => {
                setTimeout(() => {
                    // Sign Agreement
                    let seller;
                    let buyer;
                    let elevator;

                    for (const participant of agreement.participants) {
                        switch (participant.role) {
                            case 'seller':
                                seller = participant;
                                break;
                            case 'buyer':
                                buyer = participant;
                                break;
                            case 'destination':
                                elevator = participant
                                break;
                        }
                    }

                    // Load
                    // @ts-ignore
                    let load = getLoad(seller._id, elevator._id, agreement.id);
                    console.log(`Creating Load (${load.id})...`);
                    post('Load', load).then(res => {
                        load = res.data;
                        setTimeout(() => {
                            // Batch
                            const batch: any = getBatch(agreement.id, [load]);
                            console.log(`Creating Batch (${batch.id})...`);
                            post('Batch', batch).then(batchRes => {
                                console.log(batchRes.data)
                            }).catch(err => console.log(err));
                        }, timeout);

                    })
                }, timeout);
            })
        }, timeout);
    })
})();
