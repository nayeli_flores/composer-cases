import * as request from "request-promise-native";
import {crsApi} from "./settings";

export const get = async (model: string, id = ''): Promise<any> => {
    const complement = id ? `${model}/${id}` : `${model}`
    const res = await request.get(`${crsApi}/${complement}`);
    return Promise.resolve(JSON.parse(res));
}

export const post = (model: string, data: any): Promise<any> => {
    const options = {
        method: 'POST',
        uri: `${crsApi}/${model}`,
        body: data,
        json: true // Automatically stringifies the body to JSON
    };
    return request.post(options);
}
