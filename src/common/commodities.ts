import {get, post} from "./request";

export const commodities = [
    {
        "$class": "io.grainchain.Commodity",
        "id": "1",
        "name": "Yellow Corn",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "10",
        "name": "Azteca White Corn",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "11",
        "name": "Cottonseed",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "12",
        "name": "Cottonseed meal",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "13",
        "name": "Cottonseed wholes",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "14",
        "name": "Sunflower",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "15",
        "name": "DDG",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "16",
        "name": "Citrus Pellets",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "17",
        "name": "Barite",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "18",
        "name": "Cottonseed Hulls",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "19",
        "name": "Corn Screenings",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "2",
        "name": "Milo",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "20",
        "name": "PELLET",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "21",
        "name": "Rice",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "22",
        "name": "Coffee Beans",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "3",
        "name": "Black Beans",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "4",
        "name": "Pinto Beans",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "5",
        "name": "Soybeans",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "6",
        "name": "White Corn",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "7",
        "name": "Yellow Corn",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "8",
        "name": "Azteca Yellow Corn",
        "removed": false
    },
    {
        "$class": "io.grainchain.Commodity",
        "id": "9",
        "name": "Cracked Corn (yellow)",
        "removed": false
    }
]

export const initCommodities = async () => {
    const res = await get('Commodity');
    if (!res || !res.length) {
        console.log('Creating commodities...');
        await post('Commodity', commodities);
    }
}
